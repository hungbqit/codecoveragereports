@ECHO OFF

REM OpenCover-VSTest.bat

REM Run opencover against MSTest tests in your test project and show report of code coverage

REM Derivative work based on work by: 
REM  Shaun Wilde - https://www.nuget.org/packages/OpenCover/
REM  Daniel Palme - https://www.nuget.org/packages/ReportGenerator/
REM  Allen Conway - 
REM   http://www.allenconway.net/2015/06/using-opencover-and-reportgenerator-to.html
REM  Andrew Newton - 
REM   http://www.nootn.com.au/2014/01/code-coverage-with-opencover-example.html#.VxiNn_krLDc

REM Set args with multiple test dll files
SET TestDlls[0]=%~dp0output\WebApplication1.Tests.dll
SET TestDlls[1]=%~dp0output\WebApplication2.Tests.dll
SET TestDlls[2]=%~dp0output\WebApplication3.Tests.dll

SET args=
FOR /L %%i IN (0 1 2) DO  (
	IF not "%%TestDlls[%%i]%%" == "" (
		CALL SET "args=%%args%% -targetargs:\"%%TestDlls[%%i]%%\""
	)
)

REM *** IMPORTANT - Change DllContainingTests variable (above) to point to the DLL  in your solution containing your NUnit tests
REM ***
REM ***             You may also want to change the include/exclude filters  (below) for OpenCover
REM ***
REM ***             This batch file should dbe placed in the root folder of your solution

REM *** Before being able to use this to generate coverage reports you will 
REM *** need the following NuGet packages
REM PM> Install-Package OpenCover
REM PM> Install-Package ReportGenerator
REM

REM XUnit Test Runner (done this way so we dont have to change the code  when the version number changes)
for /R "%~dp0packages" %%a in (*) do if /I "%%~nxa"=="xunit.console.exe" SET TestRunnerExe=%%~dpnxa

REM Get OpenCover Executable (done this way so we dont have to change the code when the version number changes)
for /R "%~dp0packages" %%a in (*) do if /I "%%~nxa"=="OpenCover.Console.exe" SET OpenCoverExe=%%~dpnxa

REM You can set fixed path for OpenCover
REM *** SET OpenCoverExe=%HOMEPATH%\source\repos\WebApplication1\packages\OpenCover.4.7.922\tools\OpenCover.Console.exe

REM Get Report Generator (done this way so we dont have to change the when the version number changes)
for /R "%~dp0packages" %%a in (*) do if /I "%%~nxa"=="ReportGenerator.exe" SET ReportGeneratorExe=%%~dpnxa

REM You can set fixed path for ReportGenerator
REM *** SET ReportGeneratorExe=%HOMEPATH%\source\repos\WebApplication1\packages\ReportGenerator.4.6.1\tools\net47\ReportGenerator.exe

REM Create a 'GeneratedReports' folder if it does not exist
if not exist "%~dp0Reports" mkdir "%~dp0Reports"

REM Run the tests against the targeted output
call :RunOpenCoverUnitTestMetrics

REM Generate the report output based on the test results
if %errorlevel% equ 0 ( 
 call :RunReportGeneratorOutput 
)

REM Launch the report
if %errorlevel% equ 0 ( 
 call :RunLaunchReport 
)
exit /b %errorlevel%

:RunOpenCoverUnitTestMetrics 
REM *** Change the filter to include/exclude parts of the solution you want to 
REM *** check for test coverage
"%OpenCoverExe%" ^
 -target:"%TestRunnerExe%" ^
 %args% ^
 -targetdir:"%~dp0output" ^
 -filter:"+[*]* -[xunit*]* -[nunit*]* -[Antlr3*]* -[*.Tests*]* -[*]*.Global -[*]*.*Config -[*]*.*Application -[*]*.Areas.*" ^
 -output:"%~dp0Reports\XUnit_Report.xml" ^
 -mergebyhash ^
 -skipautoprops ^
 -register:user ^
 -oldStyle
exit /b %errorlevel%

:RunReportGeneratorOutput
"%ReportGeneratorExe%" ^
 -reports:"%~dp0Reports\XUnit_Report.xml" ^
 -targetdir:"%~dp0Reports\Output"
exit /b %errorlevel%

:RunLaunchReport
start "report" "%~dp0Reports\Output\index.htm"
exit /b %errorlevel%