@ECHO OFF

REM script.bat
REM Run opencover against MSTest tests & XUnit tests in your test project and show report of code coverage

REM *** IMPORTANT - Before being able to use this to generate coverage reports you will  need the following NuGet packages
REM PM> Install-Package OpenCover
REM PM> Install-Package ReportGenerator 

REM *********************************************************************************************************************************

REM *** MSTest Test Runner
SET MSTestRunnerExe=%PROGRAMFILES(X86)%\Microsoft Visual Studio 14.0\Common7\IDE\MSTest.exe

REM *** XUnit Runner
for /R "%~dp0packages" %%a in (*) do if /I "%%~nxa"=="xunit.console.exe" SET XUnitRunnerExe=%%~dpnxa

REM NUnit Test Runner (done this way so we dont have to change the code  when the version number changes)
for /R "%~dp0packages" %%a in (*) do if /I "%%~nxa"=="nunit3-console.exe" SET NUnitRunnerExe=%%~dpnxa

REM *********************************************************************************************************************************

REM Get OpenCover Executable (done this way so we dont have to change the code when the version number changes)
for /R "%~dp0packages" %%a in (*) do if /I "%%~nxa"=="OpenCover.Console.exe" SET OpenCoverExe=%%~dpnxa

REM You can also set fixed path for OpenCover
REM *** SET OpenCoverExe=%HOMEPATH%\source\repos\WebApplication1\packages\OpenCover.4.7.922\tools\OpenCover.Console.exe

REM Get Report Generator (done this way so we dont have to change the when the version number changes)
for /R "%~dp0packages" %%a in (*) do if /I "%%~nxa"=="ReportGenerator.exe" SET ReportGeneratorExe=%%~dpnxa

REM You can also set fixed path for ReportGenerator
REM *** SET ReportGeneratorExe=%HOMEPATH%\source\repos\WebApplication1\packages\ReportGenerator.4.6.1\tools\net47\ReportGenerator.exe

REM *********************************************************************************************************************************

REM Set args with multiple test dll files
SET TestDlls[0]=%~dp0output\WebApplication1.Tests.dll
SET TestDlls[1]=%~dp0output\WebApplication2.Tests.dll
SET TestDlls[2]=%~dp0output\WebApplication3.Tests.dll

REM *** args1 is used for MSTest Runner
SET args1=
FOR /L %%i IN (0 1 2) DO  (
	IF not "%%TestDlls[%%i]%%" == "" (
		CALL SET "args1=%%args1%% -targetargs:"/noisolation /testcontainer:\"%%TestDlls[%%i]%%\"""
	)
)

REM *** args2 is used for XUnit Runner
SET args2=
FOR /L %%i IN (0 1 2) DO  (
	IF not "%%TestDlls[%%i]%%" == "" (
		CALL SET "args2=%%args2%% -targetargs:\"%%TestDlls[%%i]%%\""
	)
)

REM *** args3 is used for NUnit Runner
SET args3=
FOR /L %%i IN (0 1 2) DO  (
	IF not "%%TestDlls[%%i]%%" == "" (
		CALL SET "args3=%%args3%% -targetargs:"--skipnontestassemblies \"%%TestDlls[%%i]%%\"""
	)
)

REM Create a 'GeneratedReports' folder if it does not exist
if not exist "%~dp0Reports" mkdir "%~dp0Reports"

REM *********************************************************************************************************************************

REM Run the tests against the targeted output
call :RunOpenCoverMSUnitTestMetrics
call :RunOpenCoverXUnitTestMetrics
call :RunOpenCoverNUnitTestMetrics

REM Generate the report output based on the test results
if %errorlevel% equ 0 ( 
 call :RunReportGeneratorOutput 
)

REM Launch the report
if %errorlevel% equ 0 ( 
 call :RunLaunchReport 
)
exit /b %errorlevel%

:RunOpenCoverMSUnitTestMetrics 
REM *** Change the filter to include/exclude parts of the solution you want to check for test coverage
"%OpenCoverExe%" ^
 -target:"%MSTestRunnerExe%" ^
 %args1% ^
 -filter:"+[*]* -[xunit*]* -[nunit*]* -[Antlr3*]* -[*.Tests*]* -[*]*.Global -[*]*.*Config -[*]*.*Application -[*]*.Areas.*" ^
 -output:"%~dp0Reports\MSTest_Report.xml" ^
 -mergebyhash ^
 -skipautoprops ^
 -register:user
exit /b %errorlevel%

:RunOpenCoverXUnitTestMetrics 
REM *** Change the filter to include/exclude parts of the solution you want to check for test coverage
"%OpenCoverExe%" ^
 -target:"%XUnitRunnerExe%" ^
 -targetdir:"%~dp0output" ^
 %args2% ^
 -filter:"+[*]* -[xunit*]* -[nunit*]* -[Antlr3*]* -[*.Tests*]* -[*]*.Global -[*]*.*Config -[*]*.*Application -[*]*.Areas.*" ^
 -output:"%~dp0Reports\XUnit_Report.xml" ^
 -mergebyhash ^
 -skipautoprops ^
 -register:user
exit /b %errorlevel%

:RunOpenCoverNUnitTestMetrics 
REM *** Change the filter to include/exclude parts of the solution you want to  check for test coverage
"%OpenCoverExe%" ^
 -target:"%NUnitRunnerExe%" ^
 %args3% ^
 -filter:"+[*]* -[xunit*]* -[nunit*]* -[Antlr3*]* -[*.Tests*]* -[*]*.Global -[*]*.*Config -[*]*.*Application -[*]*.Areas.*" ^
 -output:"%~dp0Reports\NUnit_Report.xml" ^
 -mergebyhash ^
 -skipautoprops ^
 -register:user
exit /b %errorlevel%

:RunReportGeneratorOutput
"%ReportGeneratorExe%" ^
 -reports:"%~dp0Reports\MSTest_Report.xml";"%~dp0Reports\XUnit_Report.xml";"%~dp0Reports\NUnit_Report.xml" ^
 -targetdir:"%~dp0Reports\Output"
exit /b %errorlevel%

:RunLaunchReport
start "report" "%~dp0Reports\Output\index.htm"
exit /b %errorlevel%